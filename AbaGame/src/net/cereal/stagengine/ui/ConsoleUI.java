package net.cereal.stagengine.ui;

import java.awt.Color;
import java.util.Scanner;

public class ConsoleUI implements IGameUI {

	public String text = "";
	private final Scanner cin;

	public ConsoleUI() {
		cin = new Scanner(System.in);
	}

	@Override
	public void append(String s) {
		text += s;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String s) {
		text = s;
	}

	@Override
	public String getInput() {
		return cin.nextLine();
	}

	@Deprecated
	@Override
	public void setInputEnabled(boolean enabled) {
	}

	@Deprecated
	@Override
	public void setBackgroundColor(Color c) {
	}

	@Deprecated
	@Override
	public void setForegroundColor(Color c) {
	}

}
