package net.cereal.stagengine.ui;

import java.awt.Color;

public interface IGameUI {

	public void append(String s);

	public default void appendln(String s) {
		append(s);
		append("\n");
	}

	public default void appendln() {
		append("\n");
	}

	public default void append(Object o) {
		append(o.toString());
	}

	public default void appendln(Object o) {
		append(o.toString());
		append("\n");
	}

	public default void appendf(String s, Object... o) {
		append(String.format(s, o));
	}

	public String getText();

	public void setText(String s);

	public String getInput();

	public void setInputEnabled(boolean enabled);

	public void setBackgroundColor(Color c);

	public void setForegroundColor(Color c);
}
