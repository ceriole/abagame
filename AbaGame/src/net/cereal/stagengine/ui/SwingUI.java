package net.cereal.stagengine.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

public class SwingUI extends JFrame implements IGameUI {
	private static final long serialVersionUID = 1L;

	private Vector<String> input = new Vector<>();
	private boolean inputEnabled;

	private JTextArea jtaContent;
	private JTextField jtfInput;
	private JButton jbConfirm;
	private JScrollPane jspContent;
	private JPanel jpStatus, jpInput;

	public SwingUI(String title, Font f, int minWidth, int minHeight) {
		super(title);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		}

		ActionListener inputAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jtfInput.getText().trim().isEmpty())
					return;
				synchronized (input) {
					input.add(jtfInput.getText());
					input.notify();
					jtfInput.setText("");
				}
			}
		};

		jtaContent = new JTextArea();
		jtaContent.setEditable(false);
		jtaContent.setFont(f);
		jtaContent.setLineWrap(true);
		jtaContent.setWrapStyleWord(true);
		jtaContent.setEditable(false);
		jspContent = new JScrollPane(jtaContent);
		jspContent.setBorder(new EmptyBorder(5, 5, 5, 5));
		jspContent.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		jspContent.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		jtfInput = new JTextField();
		jtfInput.setFont(f);
		jtfInput.addActionListener(inputAction);

		jbConfirm = new JButton("Execute");
		jbConfirm.setFont(f);
		jbConfirm.addActionListener(inputAction);

		jpStatus = new JPanel();

		setLayout(new BorderLayout());
		add(jspContent, BorderLayout.CENTER);

		jpInput = new JPanel(new BorderLayout());
		jpInput.setBorder(new EmptyBorder(5, 5, 5, 5));
		jpInput.add(jtfInput, BorderLayout.CENTER);
		jpInput.add(jbConfirm, BorderLayout.LINE_END);

		add(jpInput, BorderLayout.PAGE_END);
		add(jpStatus, BorderLayout.PAGE_START);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(minWidth, minHeight));
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Override
	public void append(String s) {
		jtaContent.append(s);
		jtaContent.setCaretPosition(jtaContent.getText().length());
	}

	@Override
	public String getText() {
		return jtaContent.getText();
	}

	@Override
	public void setText(String s) {
		jtaContent.setText(s);
	}

	@Override
	public String getInput() {
		try {
			synchronized (input) {
				input.clear();
				while (input.isEmpty())
					input.wait();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return input.get(0);
	}

	@Override
	public void setInputEnabled(boolean enabled) {
		if (inputEnabled == enabled)
			return;
		inputEnabled = enabled;
		jtfInput.setEnabled(inputEnabled);
		jbConfirm.setEnabled(inputEnabled);
	}

	@Override
	public void setBackgroundColor(Color c) {
		getContentPane().setBackground(c);
		jspContent.setBackground(c);
		jtaContent.setBackground(c);
		jpInput.setBackground(c.brighter().brighter());
		jtfInput.setBackground(c);
		jbConfirm.setBackground(c);
		if (jpStatus != null) {
			jpStatus.setBackground(c.brighter().brighter());
			for (Component comp : jpStatus.getComponents())
				comp.setBackground(c.brighter().brighter());
		}
	}

	@Override
	public void setForegroundColor(Color c) {
		getContentPane().setForeground(c);
		jspContent.setForeground(c);
		jtaContent.setForeground(c);
		jpInput.setForeground(c);
		jtfInput.setForeground(c);
		jbConfirm.setForeground(c);
		if (jpStatus != null) {
			jpStatus.setForeground(c);
			for (Component comp : jpStatus.getComponents())
				comp.setForeground(c);
		}
	}

	public void setStatusPanel(JPanel statusPanel) {
		remove(jpStatus);
		jpStatus = statusPanel;
		if (jpStatus != null)
			add(jpStatus, BorderLayout.PAGE_START);
	}

}
