package net.cereal.abagame;

public class DiceRoll {

	public int amt, range, plus;

	public DiceRoll(int amt, int range, int plus) {
		this.amt = amt;
		this.range = range;
		this.plus = plus;
	}

	public DiceRoll(int range, int plus) {
		this.amt = 1;
		this.range = range;
		this.plus = plus;
	}

	public DiceRoll(int range) {
		this.amt = 1;
		this.range = range;
	}

	public int roll() {
		int total = 0;
		for (int i = 0; i < amt; i++) {
			total += AbaGame.random.nextInt(this.range) + 1;
		}
		return total + plus;
	}

}
