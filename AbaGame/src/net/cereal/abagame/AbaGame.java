package net.cereal.abagame;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.io.FileUtils;

import net.cereal.abagame.data.Token;
import net.cereal.abagame.world.World;
import net.cereal.abagame.world.WorldPos;
import net.cereal.stagengine.ui.SwingUI;

public class AbaGame {

	public static boolean DEBUG = true;

	public static Abakhanskya aba;
	public static SwingUI ui;
	public static JPanel statusPane;
	public static JLabel lblWgtHgt;
	public static JProgressBar prgStamina;

	public static World world;

	public static Random random;

	public AbaGame() {
		random = new Random();

		initWorld();

		Font f = new Font("Consolas", Font.PLAIN, 12);
		ui = new SwingUI("AbaGame", f, 600, 300);
		ui.setVisible(false);

		try {
			BufferedImage icon = ImageIO.read(getClass().getResourceAsStream("/abaicon.png"));
			ui.setIconImage(icon);
		} catch (IOException e) {
			e.printStackTrace();
		}

		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		statusPane = new JPanel();
		statusPane.setLayout(gridbag);

		statusPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		prgStamina = new JProgressBar(0, 500);
		prgStamina.setFont(f);
		prgStamina.setString("Stamina");
		prgStamina.setStringPainted(true);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.5;
		gridbag.setConstraints(prgStamina, c);
		statusPane.add(prgStamina);

		lblWgtHgt = new JLabel();
		lblWgtHgt.setFont(f);
		lblWgtHgt.setHorizontalAlignment(SwingConstants.RIGHT);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		gridbag.setConstraints(lblWgtHgt, c);
		statusPane.add(lblWgtHgt);

		ui.setStatusPanel(statusPane);
		ui.setBackgroundColor(new Color(20, 20, 20));
		ui.setForegroundColor(Color.CYAN.darker());
		update();
		ui.setVisible(true);
		intro();
		loop();
	}

	private void initWorld() {
		world = new World();

		world.loadFromXML("/world/test.xml");

		/*
		 * Room rmStart = new Room(new WorldPos(0, 0, 0), "Empty Room",
		 * "A huge room, with completely smooth bleached-white walls. The light illumminates the room from nowhere in particular."
		 * ); rmStart.addEntity(new Container("Water Bottle",
		 * "A 2 liter bottle of clear water.", 2.2, 2.0, 0.2, 15));
		 * rmStart.setExit(Direction.EAST, true); world.addRoom(rmStart);
		 * 
		 * Room rmHall0 = new Room(new WorldPos(1, 0, 0), "White Hallway",
		 * "A long hall, with no dicerning featues whatsoever.");
		 * rmHall0.setExit(Direction.WEST, true); world.addRoom(rmHall0);
		 * 
		 * Room rmHall1 = new Room(new WorldPos(1, 0, 0), "White Hallway",
		 * "A long hall, with no dicerning featues whatsoever.");
		 * rmHall1.setExit(Direction.WEST, true); world.addRoom(rmHall1);
		 */
		aba = new Abakhanskya(new WorldPos(0, 0, 0));
	}

	private void intro() {
		ui.appendln("Aba Game v0.01");
		ui.appendln("TODO intro");
		ui.appendln();
		world.getRoomAtPos(aba.getPos()).printArea();
	}

	private void loop() {
		String input = ui.getInput().trim().toLowerCase();
		while (!input.equals("quit")) {
			ui.appendf(">%s%n", input);
			Commands.handleCommand(input);
			update();
			ui.appendln();
			input = ui.getInput().trim().toLowerCase();
		}
	}

	private void update() {
		prgStamina.setValue(Math.round(500f * aba.getStaminaPercentage()));
		lblWgtHgt.setText(String.format("Level %d (%skg to next)%n Weight: %skg Height: %sm", aba.getLevel(), Double.toString((aba.getFatToNextLevel() - aba.getFatVolume()) * Abakhanskya.FAT_DENSITY), Double.toString(aba.getWeight()), Double.toString(aba.getHeight())));
		aba.tickDigestion();
	}

	public static void addMessage(String msg) {
		ui.appendln(msg);
	}

	public static void main(String[] args) {
		Token t = new Token("items");
		try {
			String tml = FileUtils.readFileToString(new File("res/text/example.tml"), Charset.forName("UTF-8"));
			t.tokenize(tml);
			for (Token c : t.getToken("item").getChildren()) {
				System.out.println(c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// new AbaGame();
	}
}
