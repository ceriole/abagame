package net.cereal.abagame.object;

public interface IPhysicalObject {

	public double getVolume();

	public double getMass();

	public default double getDensity() {
		return getMass() / getVolume();
	}

}
