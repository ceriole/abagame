package net.cereal.abagame.object;

import net.cereal.abagame.Utils;
import net.cereal.abagame.data.Token;
import net.cereal.abagame.data.TokenCarrier;

public class InventoryItem extends TokenCarrier implements IPhysicalObject {

	private String id, name;
	public boolean isProperNamed;
	public String indefinite, definite; // Definite and indefinite article. "an", "a" and "the".
	private String onUse, onEquip, onUnequip, onTimer;

	@Override
	public String toString() {
		return toString(null);
	}

	public String toString(Token token) {
		return toString(token, false, true);
	}

	public String toString(Token token, boolean the) {
		return toString(token, the, true);
	}

	public String toString(Token token, boolean the, boolean a) {
		return "";
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getOnUse() {
		return onUse;
	}

	public String getOnEquip() {
		return onEquip;
	}

	public String getOnUnequip() {
		return onUnequip;
	}

	public String getOnTimer() {
		return onTimer;
	}

	public double getMass() {
		if (hasToken("mass"))
			return getToken("mass").value;
		addToken("mass", 0);
		return 0;
	}

	@Override
	public double getVolume() {
		if (hasToken("volume"))
			return getToken("volume").value;
		addToken("volume", 0);
		return 0;
	}

	public static InventoryItem fromToken(Token item) {
		InventoryItem ni = new InventoryItem();
		ni.id = item.text.trim();
		if (item.hasToken("_n"))
			ni.name = item.getToken("_n").text;
		else
			ni.name = ni.id.replace('_', ' ');
		if (item.hasToken("_ia"))
			ni.indefinite = item.getToken("_ia").text;
		else
			ni.indefinite = Utils.getArticle(item.name);
		ni.isProperNamed = Character.isUpperCase(ni.name.charAt(0));
		if (item.hasToken("_da"))
			ni.definite = item.getToken("_da").text;
		// TODO
		return ni;
	}

}
