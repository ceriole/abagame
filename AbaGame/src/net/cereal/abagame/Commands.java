package net.cereal.abagame;

import static net.cereal.abagame.AbaGame.DEBUG;
import static net.cereal.abagame.AbaGame.aba;
import static net.cereal.abagame.AbaGame.ui;

import java.util.Arrays;
import java.util.Vector;

import net.cereal.abagame.object.InventoryItem;
import net.cereal.abagame.world.Direction;

public class Commands {

	private static final String inventorySymbols = "abcdefghijklmnopqrstuv01234567890!@#$%^&*()-_=+[]{}\\|;\"',.<>/?\\";

	public static void handleCommand(String command) {
		String[] words = command.split(" ");
		String cmd = words[0];
		String[] args = new String[0];
		if (words.length > 1)
			args = Arrays.copyOfRange(words, 1, words.length);
		if (DEBUG)
			System.out.println("Command Received:" + cmd + " : " + Arrays.toString(args));
		switch (cmd) {
		case "h":
		case "?":
		case "help":
			Commands.help(args);
			break;
		case "g":
		case "go":
			Commands.go(args);
			break;
		case "spawn":
			if (DEBUG)
				Commands.spawn(args);
			break;
		case "i":
		case "inv":
		case "inventory":
			Commands.inv(args);
			break;
		default:
			if (Direction.getFromName(cmd) != null && args.length == 0) {
				Commands.go(new String[] { cmd });
				break;
			}
			ui.appendln("No such command.");
			break;
		}
	}

	private static void help(String[] args) {
		if (args.length > 1) {
			ui.appendln("Incorrect arguments.");
			return;
		}
		if (args.length == 0)
			ui.appendln(getGeneralHelpString());
		else
			ui.appendln(getHelpString(args[0]));
	}

	private static String getGeneralHelpString() {
		StringBuilder sb = new StringBuilder();
		sb.append("help [command]\n");
		sb.append("Aliases: ?, h\n");
		sb.append("\t");
		sb.append("Show the arguments and description of a command. If no command is given, display this.");
		return sb.toString();
	}

	private static String getHelpString(String cmd) {
		StringBuilder sb = new StringBuilder();
		switch (cmd) {
		case "h":
		case "?":
		case "help":
			sb.append(getGeneralHelpString());
			break;
		case "g":
		case "go":
			sb.append("go <direction>\n");
			sb.append("Aliases: g\n");
			sb.append("\t");
			sb.append("Move the player in a direction.");
			break;
		case "spawn":
			sb.append("spawn <item id>\n");
			sb.append("\t");
			sb.append("DEBUG ONLY! Spawns an item, dropping it into the current area the player is in.");
			break;
		case "i":
		case "inv":
		case "inventory":
			sb.append("inventory [inventory symbol]\n");
			sb.append("Aliases: i, inv\n");
			sb.append("\t");
			sb.append("Opens the item menu for the item in the player's inventory with that symbol. If no symbol is given, the player's whole inventory will be shown.");
			break;
		default:
			Direction dir = Direction.getFromName(cmd);
			if (dir != null) {
				sb.append(dir.getName());
				sb.append("\n");
				sb.append("Aliases: ");
				sb.append(dir.getOtherNamesAsList());
				sb.append("\n");
				sb.append("\t");
				sb.append("Moves player ");
				sb.append(dir.getName());
				sb.append(".");
				break;
			}
			sb.append("No such command.");
			break;
		}

		return sb.toString();
	}

	public static void go(String[] args) {
		if (args.length != 1) {
			ui.appendln("Incorrect arguments.");
			return;
		}
		Direction dir = Direction.getFromName(args[0].trim().toLowerCase());
		if (dir == null) {
			ui.appendf("'%s' is not a direction.", args[0]);
			return;
		}
		AbaGame.aba.move(AbaGame.world, dir);
	}

	public static void inv(String[] args) {
		if (args.length == 0) {
			// show inventory
			ui.appendln("Inventory:");
			Vector<InventoryItem> inv = aba.getInventory();
			for (int i = 0; i < inv.size(); i++) {
				InventoryItem item = inv.get(i);
				ui.appendf("%c - %s%n", getItemSymbol(item), item.getName());
			}
			if (inv.isEmpty())
				ui.appendln("<empty>");
			ui.appendln("=========");
			ui.appendf("%skg / %skg%n", Double.toString(aba.getCarryWeight()), Double.toString(aba.getMaxCarryWeight()));
		} else if (args.length == 1) {
			if (args[0].length() != 1) {
				ui.appendln("A single character or symbol is required.");
				return;
			}
			char invSym = args[0].charAt(0);
			InventoryItem invItem = getItemFromSymbol(invSym);
			if (invItem != null)
				showItemMenu(invItem);
			else
				ui.appendf("Inventory item with the symbol '%c' does not exist.%n", invSym);
		} else {
			ui.appendln("Too many arguments.");
		}
	}

	public static char getItemSymbol(InventoryItem i) {
		if (!aba.hasItem(i))
			return 0;
		int invIndex = aba.getItemIndexInInventory(i);
		if (invIndex < inventorySymbols.length())
			return inventorySymbols.charAt(invIndex);
		return 0;
	}

	public static InventoryItem getItemFromSymbol(char invSym) {
		int invIndex = inventorySymbols.indexOf(invSym);
		if (invIndex < 0 || invIndex > aba.getInventory().size())
			return null;
		return aba.getInventory().get(invIndex);
	}

	public static void showItemMenu(InventoryItem i) {
		boolean itemInInventory = aba.getInventory().contains(i);
		ui.appendf("What do you want to do with the %s?%n", i.getName());
		ui.appendln("e) Eat");
		if (itemInInventory)
			ui.appendln("d) Drop");
		else
			ui.appendln("g) Pick up");
		ui.appendln("c) Cancel");
		ui.appendln();
		String in = ui.getInput().trim().toLowerCase();
		ui.appendf(">%s%n", in);
		switch (in) {
		case "c":
			return;
		case "g":
			if (!itemInInventory) {
				ui.appendf("Aba grabs the %s%n", i.getName());
				aba.grab(i);
			}
			break;
		case "d":
			if (itemInInventory) {
				ui.appendf("Aba drops the %s%n", i.getName());
				aba.drop(i);
			}
			break;
		case "e":
			// TODO aba.eat(i);
			if (itemInInventory) {
				aba.removeFromInventory(i);
			}
			break;
		default:
			ui.appendln("Type an option.");
			showItemMenu(i);
			break;
		}
	}

	public static void spawn(String[] args) {
		String itemName = "";
		if (args.length < 1) {
			ui.appendln("No item given to spawn.");
			return;
		} else
			for (String a : args)
				itemName += " " + a;
		itemName = itemName.substring(1);
		InventoryItem itemToGive = null;
		switch (itemName) {
		// TODO
		}
		if (itemToGive != null) {
			ui.appendf("Poof! A %s lands into your hands.%n", itemToGive.getName());
			aba.grab(itemToGive);
		} else {
			ui.appendf("Item '%s' does not exist.%n", itemName);
		}
	}
}
