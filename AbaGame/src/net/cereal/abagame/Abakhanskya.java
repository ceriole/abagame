package net.cereal.abagame;

import static net.cereal.abagame.AbaGame.ui;

import java.util.Vector;

import net.cereal.abagame.combat.ICombatant;
import net.cereal.abagame.combat.StatusEffect;
import net.cereal.abagame.object.InventoryItem;
import net.cereal.abagame.world.Direction;
import net.cereal.abagame.world.World;
import net.cereal.abagame.world.WorldPos;

public class Abakhanskya implements ICombatant {

	// Game stuff
	public static final double BASE_MAX_STAMINA = 100.0, BASE_MAX_CARRY_WEIGHT = 350.0;
	private int level = 1;
	private double fatToNext = 200.0;
	private Vector<InventoryItem> inventory;
	private double stamina = getMaxStamina();
	private int strength, arcane, sense, ugradePoints;
	private WorldPos currentPos;

	// Physical stuff
	public static final double BASE_WEIGHT = 255.0, BASE_HEIGHT = 2.0, FAT_DENSITY = 0.8;
	// private Vector<IEdible> stomach;
	private double fatVolume = 56.25;

	public Abakhanskya(WorldPos pos) {
		inventory = new Vector<InventoryItem>();
		// stomach = new Vector<IEdible>();
		recalculateLevel();
		currentPos = pos;
	}

	public void tickDigestion() {
		/*
		 * Vector<IEdible> digested = new Vector<IEdible>(); for (IEdible e : stomach)
		 * if (e.tickDigestion()) { digested.add(e);
		 * AbaGame.ui.appendln(e.getDigestionString()); fatVolume += e.getFatVolume();
		 * recalculateLevel(); } stomach.removeAll(digested);
		 */
	}

	public double getStomachVolume() {
		double vol = 0;
		/*
		 * for (IEdible e : stomach) vol += e.getVolume();
		 */
		return vol;
	}

	public double getStomachWeight() {
		double wgt = 0;
		/*
		 * for (IEdible e : stomach) wgt += e.getWeight();
		 */
		return wgt;
	}

	public double getFatVolume() {
		return fatVolume;
	}

	public double getFatWeight() {
		return getFatVolume() * FAT_DENSITY;
	}

	public double getWeight() {
		return BASE_WEIGHT + getStomachWeight() + getFatWeight();
	}

	public double getHeight() {
		return BASE_HEIGHT;
	}

	public double getInventoryWeight() {
		double wgt = 0;
		for (InventoryItem i : inventory)
			wgt += i.getMass();
		return wgt;
	}

	public double getCarryWeight() {
		return getInventoryWeight() + getStomachWeight();
	}

	public double getMaxCarryWeight() {
		return BASE_MAX_CARRY_WEIGHT + (50.0 * (level - 1));
	}

	public boolean isOverencumbered() {
		return getCarryWeight() > getMaxCarryWeight();
	}

	public int getLevel() {
		return level;
	}

	public double getFatToNextLevel() {
		return fatToNext - fatVolume;
	}

	public double heal(double amt) {
		double i = stamina;
		amt = Math.max(0, amt);
		stamina += amt;
		if (stamina > getMaxStamina())
			stamina = getMaxStamina();
		return stamina - i;
	}

	public double damage(double amt) {
		double i = stamina;
		amt = Math.max(0, amt);
		stamina -= amt;
		if (stamina < 0)
			stamina = 0;
		return i - stamina;
	}

	private void recalculateLevel() {
		while (fatVolume > fatToNext) {
			float oldStamPerc = getStaminaPercentage();
			level++;
			fatToNext += 150.0 * level;
			stamina = oldStamPerc * getMaxStamina();
		}
	}

	public boolean isKnockedOut() {
		return stamina == 0;
	}

	public boolean grab(InventoryItem i) {
		if (getCarryWeight() + i.getMass() > getMaxCarryWeight())
			return false;
		return inventory.add(i);
	}

	public boolean drop(InventoryItem i) {
		if (inventory.contains(i))
			return inventory.remove(i);
		return false;
	}
	/*
	 * public boolean eat(IEdible e) { AbaGame.ui.appendln(e.getEatenString());
	 * return stomach.add(e); }
	 */

	public double getStamina() {
		return stamina;
	}

	public double getMaxStamina() {
		return BASE_MAX_STAMINA + (50.0 * (level - 1));
	}

	public float getStaminaPercentage() {
		return (float) (getStamina() / getMaxStamina());
	}

	public Vector<InventoryItem> getInventory() {
		return inventory;
	}

	/*
	 * public Vector<IEdible> getStomach() { return stomach; }
	 */

	public boolean hasItem(InventoryItem i) {
		return inventory.contains(i);
	}

	public int getItemIndexInInventory(InventoryItem i) {
		return inventory.indexOf(i);
	}

	public boolean removeFromInventory(InventoryItem i) {
		return inventory.remove(i);
	}

	public WorldPos getPos() {
		return currentPos;
	}

	public void move(World world, Direction dir) {
		WorldPos newPos = getPos().getPosAtDir(dir);
		if (world.getRoomAtPos(newPos) != null && world.getRoomAtPos(currentPos).canExit(dir)) {
			currentPos = newPos;
			world.getRoomAtPos(newPos).printArea();
		} else {
			ui.appendf("Cannot go %s.%n", dir.name().toLowerCase());
		}
	}

	@Override
	public int getStrength() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getArcane() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getSense() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double rollAttack() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double damage(ICombatant source, double amt) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isDefeated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addStatusEffect(StatusEffect effect) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Vector<StatusEffect> getStatusEffects() {
		// TODO Auto-generated method stub
		return null;
	}

}
