package net.cereal.abagame;

import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	private static final String vowels = "AEIOUaeiou";

	public static boolean startsWithVowel(String word) {
		word = word.trim().toLowerCase();
		if (word.isEmpty())
			return false;
		String firstLetter = word.substring(0, 1);
		return vowels.contains(firstLetter);
	}

	public static String getArticle(String word) {
		return (startsWithVowel(word) ? "an" : "a");
	}

	@SafeVarargs
	public static <T> Vector<T> createVector(T... elements) {
		Vector<T> vec = new Vector<T>();
		for (T o : elements)
			vec.add(o);
		return vec;
	}

	public static String orEmpty(String s) {
		return (s == null ? "" : s);
	}

	public static String remove(String s, int index) {
		if (index < 0 || index > s.length())
			throw new IllegalArgumentException("Index out of range!");
		StringBuilder sb = new StringBuilder(s);
		sb.delete(index, s.length());
		return sb.toString();
	}

	public static <T extends Collection<K>, K> Vector<K> where(T collection, Predicate<K> predicate) {
		Vector<K> out = new Vector<K>();
		collection.forEach((o) -> {
			if (predicate.test(o))
				out.add(o);
		});
		return out;
	}

	public static <T extends Collection<K>, K> K firstMatch(T collection, Predicate<K> predicate) {
		for (K o : collection)
			if (predicate.test(o))
				return o;
		return null;
	}

	public static String parseEscapes(String text) {
		Pattern entity = Pattern.compile("&#x([A-Za-z0-9]{2,4});");
		Matcher matcher = entity.matcher(text);
		while (matcher.find()) {
			String replacement = new String("" + (char) Integer.parseInt(matcher.group().substring(3, matcher.group().length() - 4)));
			text = text.replace(matcher.group(), replacement);
		}
		return text;
	}

	public static DiceRoll parseRoll(String text) {
		int amt = 1, range = 0, plus = 0;
		Matcher m = Pattern.compile("(\\d+)d(\\d+)\\+(\\d+)").matcher(text);
		if (!m.matches()) {
			m = Pattern.compile("(\\d+)d(\\d+)").matcher(text);
			if (!m.matches())
				return null;
		}
		amt = Integer.parseInt(m.group(1));
		range = Integer.parseInt(m.group(2));
		if (m.groupCount() == 4)
			plus = Integer.parseInt(m.group(3));
		return new DiceRoll(amt, range, plus);
	}

	public static <T extends List<K>, K> K pickOne(T collection) {
		return collection.get(AbaGame.random.nextInt(collection.size()));
	}

	public static <K> K pickOne(K[] array) {
		return array[AbaGame.random.nextInt(array.length)];
	}

	public static String trimStart(String str) {
		return str.replaceAll("^\\s+", "");
	}

	public static String trimEnd(String str) {
		return str.replaceAll("\\s+$", "");
	}

	public static String titleCase(String str) {
		StringBuilder titleCase = new StringBuilder();
		boolean nextTitleCase = true;

		for (char c : str.toCharArray()) {
			if (Character.isSpaceChar(c)) {
				nextTitleCase = true;
			} else if (nextTitleCase) {
				c = Character.toTitleCase(c);
				nextTitleCase = false;
			}

			titleCase.append(c);
		}

		return titleCase.toString();
	}

}
