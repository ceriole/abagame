package net.cereal.abagame.data;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BinaryReader {

	private InputStream stream;

	public BinaryReader(InputStream stream) {
		this.stream = stream;
	}

	public BinaryReader(byte[] data) {
		stream = new ByteArrayInputStream(data);
	}

	public byte read() throws IOException {
		return (byte) stream.read();
	}

	public void read(byte[] buf) throws IOException {
		stream.read(buf);
	}

	public short readShort() throws IOException {
		byte[] buf = new byte[Short.BYTES];
		read(buf);
		return SSSerialization.readShort(buf, 0);
	}

	public byte readByte() throws IOException {
		return read();
	}

	public int readInt() throws IOException {
		byte[] buf = new byte[Integer.BYTES];
		read(buf);
		return SSSerialization.readInteger(buf, 0);
	}

	public float readFloat() throws IOException {
		byte[] buf = new byte[Float.BYTES];
		read(buf);
		return SSSerialization.readFloat(buf, 0);
	}

	public long readLong() throws IOException {
		byte[] buf = new byte[Long.BYTES];
		read(buf);
		return SSSerialization.readLong(buf, 0);
	}

	public double readDouble() throws IOException {
		byte[] buf = new byte[Double.BYTES];
		read(buf);
		return SSSerialization.readDouble(buf, 0);
	}

	public String readString() throws IOException {
		short strLen = readShort();
		byte[] buf = new byte[Byte.BYTES * strLen];
		read(buf);
		return SSSerialization.readString(buf, 0, strLen);
	}
}