package net.cereal.abagame.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import net.cereal.abagame.Utils;

public class Token extends TokenCarrier implements Cloneable {

	public String name = "";
	public float value;
	public String text = "";

	@Override
	public String toString() {
		if (text.isEmpty())
			return String.format("%s (%.2f, %d children)", name, value, tokens.size());
		return String.format("%s (%.2f, \"%s\", %d children)", name, value, text, tokens.size());
	}

	public Token() {
	}

	public Token(String name) {
		this.name = name;
	}

	public Token(String name, float value) {
		this.name = name;
		this.value = value;
	}

	public Token(String name, String text) {
		this.name = name;
		this.text = text;
	}

	public Token(String name, float value, String text) {
		this.name = name;
		this.value = value;
		this.text = text;
	}

	public void saveToFile(BinaryWriter stream) {
		stream.write(Utils.orEmpty(name));
		boolean isInt = (value == (int) value);
		int flags = 0;
		if (value != 0)
			flags |= 1;
		if (isInt)
			flags |= 2;
		if (!text.isEmpty())
			flags |= 4;
		flags |= tokens.size() << 4;
		stream.write(flags);
		if (value != 0)
			if (isInt)
				stream.write((int) value);
			else
				stream.write(value);
		if (!text.isEmpty())
			stream.write(text);
		if (tokens.size() > 0)
			for (Token t : tokens)
				t.saveToFile(stream);
	}

	public static Token loadFromFile(InputStream stream) throws IOException {
		BinaryReader reader = new BinaryReader(stream);
		Token newToken = new Token();
		newToken.name = reader.readString();
		int flags = reader.readInt();
		// Format: child count, reserved, has text, value is integral, has value.
		int numTokens = flags >> 4;
		if ((flags & 1) == 1) {
			boolean isInt = ((flags & 2) == 2);
			if (isInt)
				newToken.value = reader.readInt();
			else
				newToken.value = reader.readFloat();
		}
		if ((flags & 4) == 4)
			newToken.text = reader.readString();
		for (int i = 0; i < numTokens; i++)
			newToken.tokens.add(Token.loadFromFile(stream));
		return newToken;
	}

	public boolean isMatch(Vector<Token> otherSet) {
		for (Token toFind : this.tokens) {
			Token f = Utils.firstMatch(otherSet, (t) -> t.name == toFind.name);
			if (f == null)
				return false;
			if (toFind.tokens.size() > 0) {
				boolean contentMatch = toFind.isMatch(f.tokens);
				if (!contentMatch)
					return false;
			}
		}
		return true;
	}

	public void removeSet(Vector<Token> otherSet) {
		this.tokens.removeAll(otherSet);
	}

	public void addSet(Vector<Token> otherSet) {
		for (Token toAdd : otherSet)
			this.tokens.add(toAdd.clone());
	}

	@Override
	public Token clone() {
		Token clone = new Token(this.name, this.value, this.text);
		for (Token t : tokens)
			clone.addToken(t.clone());
		return clone;
	}

	public Token shallowClone() {
		Token clone = new Token(this.name, this.value, this.text);
		for (Token t : tokens)
			clone.addToken(t);
		return clone;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Token)
			return deepEquals((Token) obj);
		return false;
	}

	public boolean deepEquals(Token other) {
		if (other.name == this.name && other.value == this.value && other.text == this.text) {
			if (other.tokens.size() != this.tokens.size())
				return false;
			for (int i = 0; i < this.tokens.size(); i++)
				if (!this.tokens.get(i).equals(other.tokens.get(i)))
					return false;
			return true;
		}
		return false;
	}
}
