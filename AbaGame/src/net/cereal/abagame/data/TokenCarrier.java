package net.cereal.abagame.data;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.cereal.abagame.DiceRoll;
import net.cereal.abagame.Utils;

public class TokenCarrier {

	protected Vector<Token> tokens;

	public TokenCarrier() {
		tokens = new Vector<Token>();
	}

	public boolean hasToken(String name) {
		return getToken(name) != null;
	}

	public Token getToken(String name) {
		for (Token t : tokens) {
			if (t.name.equalsIgnoreCase(name))
				return t;
		}
		return null;
	}

	public Token getToken(String name, String text) {
		for (Token t : tokens) {
			if (t.name.equalsIgnoreCase(name) && t.text.equalsIgnoreCase(text))
				return t;
		}
		return null;
	}

	public Vector<Token> getAll(String name) {
		Vector<Token> out = new Vector<Token>();
		for (Token t : tokens)
			if (t.name.equalsIgnoreCase(name))
				out.add(t);
		return out;
	}

	public Token addToken(String name, float value, String text) {
		Token t = new Token(name, value, text);
		tokens.add(t);
		return t;
	}

	public Token addToken(String name, float value) {
		Token t = new Token(name, value);
		tokens.add(t);
		return t;
	}

	public Token addToken(String name) {
		Token t = new Token(name);
		tokens.add(t);
		return t;
	}

	public Token addToken(Token t) {
		tokens.add(t);
		return t;
	}

	public Token removeToken(String name) {
		Token t = getToken(name);
		if (t != null)
			tokens.remove(t);
		return t;
	}

	public Token removeToken(String name, String text) {
		Token t = getToken(name, text);
		if (t != null)
			tokens.remove(t);
		return t;
	}

	public Token removeToken(Token t) {
		tokens.remove(t);
		return t;
	}

	public Token removeToken(int index) {
		if (index < 0 || index >= tokens.size())
			throw new IllegalArgumentException("Index out of range!");
		Token t = tokens.get(index);
		tokens.remove(t);
		return t;
	}

	public void removeAll(String name) {
		tokens.removeIf((t) -> t.name.equals(name));
	}

	public Token path(String pathSpec) {
		String[] parts = pathSpec.split("/");
		TokenCarrier point = this;
		String last = parts[parts.length - 1];
		Pattern indexRegx = Pattern.compile("\\[(?<index>[0-9]+)\\]");
		Pattern textRegx = Pattern.compile("\\[=(?<text>\\w+\\]");
		Matcher indMtchr = indexRegx.matcher(last);
		Matcher textMtchr = textRegx.matcher(last);
		if (indMtchr.matches())
			last = Utils.remove(last, last.indexOf('['));
		else if (textMtchr.matches())
			last = Utils.remove(last, last.indexOf('['));

		for (String p : parts) {
			Token target = null;
			indMtchr = indexRegx.matcher(p);
			textMtchr = textRegx.matcher(p);
			if (indMtchr.matches()) {
				String trueP = Utils.remove(p, p.indexOf('['));
				int index = Integer.parseInt(indMtchr.group("index"));
				Vector<Token> targets = point.getAll(trueP);
				if (targets == null || index >= targets.size())
					return null;
				target = targets.get(index);
			} else if (textMtchr.matches()) {
				String trueP = Utils.remove(p, p.indexOf('['));
				String text = textMtchr.group("text");
				Vector<Token> possibilites = point.getAll(trueP);
				target = Utils.firstMatch(possibilites, (t) -> !t.text.isEmpty() && t.text.equalsIgnoreCase(text));
			} else
				target = point.getToken(p);

			if (target == null)
				return null;
			if (target.name.equalsIgnoreCase(last))
				return target;
			point = target;
		}
		return null;
	}

	public Token item(int index) {
		if (index < 0 || index >= tokens.size())
			throw new IllegalArgumentException("Index out of range!");
		return tokens.get(index);
	}

	public int count() {
		return tokens.size();
	}

	public void tokenize(String source) {
		Vector<Token> t = new Vector<Token>();
		Vector<String> lines = Utils.createVector(source.split("\n"));
		lines.removeIf((s) -> s.startsWith("--"));
		Vector<Token> nodes = new Vector<Token>();
		int prevTabs = 0;
		boolean cdata = false;
		StringBuilder cdataText = new StringBuilder();
		for (String line : lines) {
			String l = Utils.trimEnd(line);
			if (cdata) {
				if (l.endsWith("]]>")) {
					nodes.lastElement().addToken("#text", 0, Utils.parseEscapes(cdataText.toString()));
					cdata = false;
					continue;
				}
				cdataText.append(l.trim() + '\n');
				continue;
			} else if (line.isEmpty())
				continue;
			// count number of tabs in front
			int tabs = 0;
			for (; tabs < l.length() - 1; tabs++)
				if (l.charAt(tabs) != '\t')
					break;
			l = Utils.trimStart(l);
			Token newOne = new Token();
			String tokenName = l;
			if (tokenName.equals("<[[")) {
				// Start of a CDATA-style text block! Switch to CDATA mode, keep parsing until
				// ]]> and place it in the last node as "#text".
				cdata = true;
				cdataText = new StringBuilder();
				continue;
			}
			if (tokenName.startsWith("- ")) {
				// Token is an anonymous value -- transform from "- X" to "#a: X".
				tokenName = String.format("#a: %s", tokenName.substring(2));
				l = tokenName;
			}
			if (l.contains(": ")) {
				// Token has a value
				if (l.contains(": \"")) {
					String text = l.substring(l.indexOf('\"') + 1);
					newOne.text = Utils.remove(text, text.lastIndexOf('\"'));
				} else if (l.contains(": U+")) {
					String codePoint = l.substring(l.lastIndexOf('+') + 1);
					newOne.value = Integer.parseInt(codePoint, 16);
				} else if (l.contains(": 0x")) {
					String codePoint = l.substring(l.lastIndexOf('x') + 1);
					newOne.value = Integer.parseInt(codePoint, 16);
				} else {
					float v;
					String value = l.substring(l.indexOf(' ') + 1);
					try {
						v = Float.parseFloat(value);
						newOne.value = v;
					} catch (NumberFormatException e) {
						newOne.text = value;
					}
				}
				tokenName = Utils.remove(tokenName, tokenName.indexOf(':'));
				if (!newOne.text.isEmpty())
					newOne.text = Utils.parseEscapes(newOne.text);
			}
			newOne.name = tokenName;

			if (tabs == 0) {
				// New one here
				t.add(newOne);
				nodes.clear();
				nodes.add(newOne);
			} else if (tabs == prevTabs + 1) {
				Token hook = nodes.get(prevTabs);
				hook.tokens.add(newOne);
				nodes.add(newOne);
			} else if (tabs < prevTabs) {
				Token hook = nodes.get(prevTabs - 1);
				hook.tokens.add(newOne);
				nodes.subList(tabs, nodes.size() - tabs).clear();
				nodes.add(newOne);
			} else if (tabs == prevTabs) {
				Token hook = nodes.get(tabs - 1);
				hook.tokens.add(newOne);
				nodes.set(tabs, newOne);
			} else {
				throw new RuntimeException(String.format("Token tree contains a line that's indented too far. Line is \"%s\", indenting %d level(s), but the previous line is only %d level(s).", l.trim(), tabs, prevTabs));
			}
			prevTabs = tabs;
		}
		tokens = t;
		// noRolls = false;
	}

	public void resolveRolls() {
		for (Token token : tokens) {
			if (token.tokens.size() > 0)
				token.resolveRolls();
			if (token.text.isEmpty())
				continue;
			if (token.text.startsWith("roll ")) {
				String xDyPz = token.text.substring(token.text.lastIndexOf(' ') + 1);
				DiceRoll dr = Utils.parseRoll(xDyPz);
				int roll = dr.roll();
				token.value = roll;
				token.text = null;
			} else if (token.text.startsWith("oneof ")) {
				String[] options = token.text.substring(token.text.indexOf("of ") + 3).split(",");
				String choice = Utils.pickOne(options).trim();
				token.text = choice;
			}
		}
	}

	public void patch(String source) {
		Token patch = new Token();
		patch.tokenize(source);
		for (Token token : patch.tokens) {
			String path = token.text;
			if (token.name == "add") {
				if (path == "-") {
					tokens.addAll(token.tokens);
				} else if (path.endsWith("/-")) {
					Token target = path(path.substring(0, path.length() - 2));
					if (target != null) {
						target.tokens.addAll(token.tokens);
					}
				}
				{
					Token target = path(path);
					if (target != null) {
						tokens.addAll(tokens.indexOf(target) + 1, token.tokens);
					}
				}
			} else if (token.name == "remove") {
				Token target = path(path);
				if (target != null) {
					if (path.lastIndexOf('/') == -1) {
						tokens.remove(target);
					} else {
						Token oneUp = path(path.substring(0, path.lastIndexOf('/')));
						oneUp.tokens.remove(target);
					}
				}
			} else if (token.name == "replace") {
				Token target = path(path);
				if (target != null) {
					if (path.lastIndexOf('/') == -1) {
						tokens.set(tokens.indexOf(target), token.tokens.firstElement());
					} else {
						Token oneUp = path(path.substring(0, path.lastIndexOf('/')));
						oneUp.tokens.set(oneUp.tokens.indexOf(target), token.tokens.firstElement());
					}
				}
			} else if (token.name == "set") {
				Token target = path(path);
				if (target != null) {
					Token val = token.getToken("value");
					Token txt = token.getToken("text");
					if (val != null)
						target.value = val.value;
					if (txt != null)
						target.text = txt.text;
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<Token> getChildren() {
		return (Vector<Token>) tokens.clone();
	}

}
