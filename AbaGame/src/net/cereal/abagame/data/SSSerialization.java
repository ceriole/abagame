package net.cereal.abagame.data;

import java.nio.ByteBuffer;

public class SSSerialization {

	// WRITING
	// ==================================================================================

	public static int write(byte[] dest, int pointer, byte value) {
		assert (dest.length >= pointer + Byte.BYTES);
		dest[pointer++] = value;
		return pointer;
	}

	public static int write(byte[] dest, int pointer, short value) {
		assert (dest.length >= pointer + Short.BYTES);
		dest[pointer++] = (byte) ((value >> 8) & 0xff);
		dest[pointer++] = (byte) ((value >> 0) & 0xff);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, char value) {
		assert (dest.length >= pointer + Character.BYTES);
		dest[pointer++] = (byte) ((value >> 8) & 0xff);
		dest[pointer++] = (byte) ((value >> 0) & 0xff);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, int value) {
		assert (dest.length >= pointer + Integer.BYTES);
		dest[pointer++] = (byte) ((value >> 24) & 0xff);
		dest[pointer++] = (byte) ((value >> 16) & 0xff);
		dest[pointer++] = (byte) ((value >> 8) & 0xff);
		dest[pointer++] = (byte) ((value >> 0) & 0xff);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, long value) {
		assert (dest.length >= pointer + Long.BYTES);
		dest[pointer++] = (byte) ((value >> 56) & 0xff);
		dest[pointer++] = (byte) ((value >> 48) & 0xff);
		dest[pointer++] = (byte) ((value >> 40) & 0xff);
		dest[pointer++] = (byte) ((value >> 32) & 0xff);
		dest[pointer++] = (byte) ((value >> 24) & 0xff);
		dest[pointer++] = (byte) ((value >> 16) & 0xff);
		dest[pointer++] = (byte) ((value >> 8) & 0xff);
		dest[pointer++] = (byte) ((value >> 0) & 0xff);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, float value) {
		assert (dest.length >= pointer + Float.BYTES);
		int data = Float.floatToIntBits(value);
		return write(dest, pointer, data);
	}

	public static int write(byte[] dest, int pointer, double value) {
		assert (dest.length >= pointer + Double.BYTES);
		long data = Double.doubleToLongBits(value);
		return write(dest, pointer, data);
	}

	public static int write(byte[] dest, int pointer, boolean value) {
		assert (dest.length >= pointer + Byte.BYTES);
		dest[pointer++] = (byte) (value ? 1 : 0);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, String string) {
		assert (dest.length >= pointer + Short.BYTES + (string.length() * Byte.BYTES));
		pointer = write(dest, pointer, (short) string.length());
		return write(dest, pointer, string.getBytes());
	}

	public static int write(byte[] dest, int pointer, byte[] src) {
		assert (dest.length >= pointer + (Byte.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, short[] src) {
		assert (dest.length >= pointer + (Short.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, char[] src) {
		assert (dest.length >= pointer + (Character.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, int[] src) {
		assert (dest.length >= pointer + (Integer.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, long[] src) {
		assert (dest.length >= pointer + (Long.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, float[] src) {
		assert (dest.length >= pointer + (Float.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, double[] src) {
		assert (dest.length >= pointer + (Double.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	public static int write(byte[] dest, int pointer, boolean[] src) {
		assert (dest.length >= pointer + (Byte.BYTES * src.length));
		for (int i = 0; i < src.length; i++)
			pointer = write(dest, pointer, src[i]);
		return pointer;
	}

	// READING
	// ==================================================================================

	public static byte readByte(byte[] src, int pointer) {
		assert (src.length >= pointer + Byte.BYTES);
		return src[pointer];
	}

	public static short readShort(byte[] src, int pointer) {
		assert (src.length >= pointer + Short.BYTES);
		return ByteBuffer.wrap(src, pointer, Short.BYTES).getShort();
	}

	public static char readCharacter(byte[] src, int pointer) {
		assert (src.length >= pointer + Character.BYTES);
		return ByteBuffer.wrap(src, pointer, Character.BYTES).getChar();
	}

	public static int readInteger(byte[] src, int pointer) {
		assert (src.length >= pointer + Integer.BYTES);
		return ByteBuffer.wrap(src, pointer, Integer.BYTES).getInt();
	}

	public static long readLong(byte[] src, int pointer) {
		assert (src.length >= pointer + Long.BYTES);
		return ByteBuffer.wrap(src, pointer, Long.BYTES).getLong();
	}

	public static float readFloat(byte[] src, int pointer) {
		assert (src.length >= pointer + Float.BYTES);
		return Float.intBitsToFloat(readInteger(src, pointer));
	}

	public static double readDouble(byte[] src, int pointer) {
		assert (src.length >= pointer + Double.BYTES);
		return Double.longBitsToDouble(readLong(src, pointer));
	}

	public static boolean readBoolean(byte[] src, int pointer) {
		assert (src.length >= pointer + Byte.BYTES);
		assert (src[pointer] == 0 || src[pointer] == 1);
		return src[pointer] != 0;
	}

	public static String readString(byte[] src, int pointer, int length) {
		assert (src.length >= pointer + length);
		return new String(src, pointer, length);
	}

	public static void readBytes(byte[] src, int pointer, byte[] dest) {
		assert (src.length >= pointer + (dest.length * Byte.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readByte(src, pointer + (i * Byte.BYTES));
	}

	public static void readShorts(byte[] src, int pointer, short[] dest) {
		assert (src.length >= pointer + (dest.length * Short.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readShort(src, pointer + (i * Short.BYTES));
	}

	public static void readCharacters(byte[] src, int pointer, char[] dest) {
		assert (src.length >= pointer + (dest.length * Character.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readCharacter(src, pointer + (i * Character.BYTES));
	}

	public static void readIntegers(byte[] src, int pointer, int[] dest) {
		assert (src.length >= pointer + (dest.length * Integer.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readInteger(src, pointer + (i * Integer.BYTES));
	}

	public static void readLongs(byte[] src, int pointer, long[] dest) {
		assert (src.length >= pointer + (dest.length * Long.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readLong(src, pointer + (i * Long.BYTES));
	}

	public static void readFloats(byte[] src, int pointer, float[] dest) {
		assert (src.length >= pointer + (dest.length * Float.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readFloat(src, pointer + (i * Float.BYTES));
	}

	public static void readDoubles(byte[] src, int pointer, double[] dest) {
		assert (src.length >= pointer + (dest.length * Double.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readDouble(src, pointer + (i * Double.BYTES));
	}

	public static void readBooleans(byte[] src, int pointer, boolean[] dest) {
		assert (src.length >= pointer + (dest.length * Byte.BYTES));
		for (int i = 0; i < dest.length; i++)
			dest[i] = readBoolean(src, pointer + (i * Byte.BYTES));
	}

}