package net.cereal.abagame.world;

import java.util.Vector;

public class World {

	public Vector<Room> rooms;

	public World() {
		rooms = new Vector<Room>();
	}

	public void addRoom(Room r) {
		for (Room rm : rooms) {
			if (r.positionEquals(rm)) {
				System.err.printf("Room already exists at %s!", rm.getPos());
				return;
			}
		}
		rooms.add(r);
	}

	public Room getRoomAtPos(WorldPos pos) {
		for (Room rm : rooms) {
			if (rm.getPos().equals(pos))
				return rm;
		}
		return null;
	}

	public Room getRoomAtDir(WorldPos pos, Direction dir) {
		return getRoomAtPos(pos.getPosAtDir(dir));
	}

	public void loadFromXMLFile(String filePath) {

	}

	public void loadFromXML(String xmlSrc) {

	}

}
