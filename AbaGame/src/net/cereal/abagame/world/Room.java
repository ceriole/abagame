package net.cereal.abagame.world;

import static net.cereal.abagame.AbaGame.ui;

public class Room {

	private final WorldPos pos;

	private boolean[] exits = new boolean[Direction.values().length];
	// private Vector<IEntity> entites = new Vector<IEntity>();

	private String name, description;

	public Room(WorldPos pos, String name, String description) {
		this.pos = pos;
		this.name = name;
		this.description = description;
	}

	public Room setExit(Direction direction, boolean open) {
		exits[direction.ordinal()] = open;
		return this;
	}

	/*
	 * public Room addEntity(IEntity entity) { entites.add(entity); return this; }
	 */

	public void printArea() {
		ui.appendln(name);
		ui.appendln("========");
		ui.appendln(description);
		ui.appendln();
		/*
		 * for (IEntity e : entites) ui.appendf("There is %s %s here.%n",
		 * e.getArticle(), e.getName()); if (!entites.isEmpty()) ui.appendln();
		 */for (int d = 0; d < exits.length; d++)
			if (exits[d])
				ui.appendf("There is an exit to the %s.%n", Direction.values()[d].name().toLowerCase());
		if (hasExit())
			ui.appendln();
	}

	private boolean hasExit() {
		for (int d = 0; d < exits.length; d++)
			if (exits[d])
				return true;
		return false;
	}

	public WorldPos getPos() {
		return pos;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean positionEquals(Room rm) {
		return pos.equals(rm.getPos());
	}

	public boolean canExit(Direction dir) {
		return exits[dir.ordinal()];
	}

	/*
	 * public Vector<IEntity> getEntities() { return entites; }
	 */
}
