package net.cereal.abagame.world;

public class WorldPos implements Cloneable {

	public int x, y, z;

	public WorldPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public WorldPos getPosAtDir(Direction dir) {
		return new WorldPos(x + dir.dx, y + dir.dy, z + dir.dy);
	}

	public boolean equals(WorldPos o) {
		return x == o.x && y == o.y && z == o.z;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof WorldPos)
			return equals((WorldPos) o);
		return false;
	}

	@Override
	protected WorldPos clone() {
		return new WorldPos(x, y, z);
	}

	@Override
	public String toString() {
		return String.format("WorldPos: {%d, %d, %d}", x, y, z);
	}

}
