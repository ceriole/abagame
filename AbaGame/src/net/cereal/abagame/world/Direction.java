package net.cereal.abagame.world;

import java.util.Iterator;
import java.util.Vector;

public enum Direction {
	NORTH(0, -1, 0, "north", "n"), EAST(1, 0, 0, "east", "e"), SOUTH(0, 1, 0, "south", "s"), WEST(-1, 0, 0, "west", "w"), UP(0, 0, 1, "up", "u"), DOWN(0, 0, -1, "down", "d");

	public final int dx, dy, dz;
	private Vector<String> names;

	private Direction(int dx, int dy, int dz, String... names) {
		this.dx = dx;
		this.dy = dy;
		this.dz = dz;
		this.names = new Vector<String>();
		for (String n : names)
			this.names.add(n.toLowerCase());
	}

	public boolean isName(String name) {
		return names.contains(name.trim().toLowerCase());
	}

	public static Direction getFromName(String name) {
		for (Direction d : values())
			if (d.isName(name))
				return d;
		return null;
	}

	public String getName() {
		return names.firstElement();
	}

	public String getShortName() {
		return names.lastElement();
	}

	public String getOtherNamesAsList() {
		StringBuilder sb = new StringBuilder();
		Iterator<String> iterNames = names.iterator();
		iterNames.next();
		iterNames.forEachRemaining((n) -> {
			sb.append(n);
			if (iterNames.hasNext())
				sb.append(", ");
		});
		return sb.toString();
	}
}
