package net.cereal.abagame;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.script.ScriptException;

import org.apache.commons.io.FileUtils;
import org.luaj.vm2.script.LuaScriptEngine;
import org.luaj.vm2.script.LuaScriptEngineFactory;

public class Lua {

	public static LuaScriptEngine environment;

	public static LuaScriptEngine create() {
		LuaScriptEngineFactory lsef = new LuaScriptEngineFactory();
		environment = (LuaScriptEngine) lsef.getScriptEngine();
		return environment;
	}

	public static void acertain() {
		acertain(environment);
	}

	public static void acertain(LuaScriptEngine env) {
		if (env.get("aba") == null && AbaGame.aba != null)
			env.put("aba", AbaGame.aba);

		if (env.get("acertained") != null)
			return;

		// TODO more of this stuff

		runFile("data/init.lua");

		env.put("runFile", (Function<String, Object>) Lua::runFile);

		env.put("addMessage", (Consumer<String>) AbaGame::addMessage);
		env.put("titleCase", (Function<String, String>) Utils::titleCase);
		env.put("startsWithVowel", (Function<String, Boolean>) Utils::startsWithVowel);

		env.put("acertained", true);
	}

	private static String prepareParseError(String block, int line, int column) {
		String[] lines = block.split("\n");
		if (lines.length == 1)
			return lines[0].trim() + " <---";
		int first = line - 4;
		int last = line + 4;
		if (first < 0)
			first = 0;
		if (last > lines.length)
			last = lines.length;
		StringBuilder ret = new StringBuilder();
		for (int i = first; i < last; i++) {
			ret.append(lines[i].trim());
			if (i == line - 1)
				ret.append(" <---");
			ret.append('\n');
		}
		return ret.toString();
	}

	public static Object runFile(String file) {
		return runFile(file, environment);
	}

	public static Object runFile(String file, LuaScriptEngine env) {
		try {
			return run(FileUtils.readFileToString(new File(file), Charset.forName("UTF-8")), env);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object run(String script) {
		return run(script, environment);
	}

	public static Object run(String script, LuaScriptEngine env) {
		try {
			return env.eval(script);
		} catch (ScriptException e) {
			throw new RuntimeException("Lua error while trying to run this script: \n" + prepareParseError(script, e.getLineNumber(), e.getColumnNumber()) + "\n" + e.getMessage(), e);
		}
	}

}
