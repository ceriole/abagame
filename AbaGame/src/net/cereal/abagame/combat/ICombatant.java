package net.cereal.abagame.combat;

import java.util.Vector;

public interface ICombatant {

	public static final int BASE_STAT = 20;

	public int getLevel();

	public int getStrength();

	public int getArcane();

	public int getSense();

	public static int getStatBonus(int stat) {
		return (stat - BASE_STAT) / 2;
	}

	public default double getDefense(ICombatant source) {
		return (Math.max(0, (source.getSense() - getSense())) * 0.10) + (Math.max(0, (source.getStrength() - getStrength())) * 0.10);
	}

	public double rollAttack();

	public double damage(ICombatant source, double amt);

	public double heal(double amt);

	public boolean isDefeated();

	public boolean addStatusEffect(StatusEffect effect);

	public Vector<StatusEffect> getStatusEffects();

}
