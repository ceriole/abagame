function getArticle(topic)
	if startsWithVowel(topic) then
		return "an"
	else
		return "a"
	end
end

local regular = {
	rtex = "rtices",
	cubus = "cubi",
	ooth = "eeth",
	oof = "ooves",
	fe = "ves",
	--  Specify sus and lus instead of just us because house -> houses -> hous 
	sus = "suses",
	lus = "luses",
	bus = "buses",
	cus = "ci",
	se = "ses",
	ch = "ches",
	oy = "oys",
	x = "xes",
	y = "ies",
}

function pluralize(singular)
	local of = singular:find(" of ")
	if (of) then
		local key = singular:sub(1, of - 1)
		local ofWhat = singular:sub(of)
		return pluralize(key) .. ofWhat
	end

	for _, value in pairs(uncountable) do
		if value == singular then
			return singular
		end
	end


	for left,right in pairs(irregular) do
		if right:sub(1, 1) == "!" then
			if singular == left then
				return right:sub(1)
			end
		end

		if singular:sub(-(left:len())) == left then
			return singular:sub(1, -(left:len()) - 1) .. right
		end
	end
	
	for left,right in pairs(regular) do
		if singular:sub(-(left:len())) == left then
			return singular:sub(1, -(left:len()) - 1) .. right
		end
	end

	return singular .. "s"
end

function singularize(plural)
	local of = plural:find(" of ")
	if (of) then
		local key = plural:sub(1, of - 1)
		local ofWhat = plural:sub(of)
		return singularize(key) .. ofWhat
	end

	for _, value in pairs(uncountable) do
		if value == plural then
			return plural
		end
	end

	for left,right in pairs(irregular) do
		if right:sub(1, 1) == "!" then
			if plural == right:sub(2) then
				return left
			end
		end

		if plural:sub(-(right:len())) == right then
			return plural:sub(1, -(right:len()) - 1) .. left
		end
	end
	
	for left,right in pairs(regular) do
		if plural:sub(-(right:len())) == right then
			return plural:sub(1, -(right:len()) - 1) .. left
		end
	end

	return plural:sub(1, -2)
end

function possessive(subject)
	if subject == "it" then return "its" end
	if subject:sub(-1) == "s" then return subject .. "'" end
	return subject .. "'s"
end

function ordinal(number)
	local i = tonumber(number)
	local dd = i % 10
	if (dd == 0) or (dd > 3) or ((i % 100) / 10 == 1) then return i .. "th" end
	if (dd == 1) then return i .. "st" end
	if (dd == 2) then return i .. "nd" end
	return i .. "rd"
end